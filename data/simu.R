library(ggplot2)

finres <- readRDS('C:\\Users\\sveng\\OneDrive - University of Tasmania\\Nextcloud\\GitHub\\simsig\\data\\simu.RDS')
finres <- data.frame()
SNR <- seq(-20,10,by=0.3)

for( reps in 1:20){
  message(reps)
  for(d in seq(0.1,35,by=0.3)){
  for(snr in SNR){
    res <- pulseGenSize(ramp="fast", SNR=snr,d0=d, res.only = TRUE)
    tmp <- res$results
    tmp$res <- snr
    finres <- rbind(finres,tmp)
  }
}
}
library(ggplot2)
f<- finres



finres <- f
finres <- finres[is.na(finres$dest)==FALSE,]
finres$error <- NA
finres$error[is.na(finres$dest)==FALSE] <- (finres[is.na(finres$dest)==FALSE ,]$dest - 
                                              finres[is.na(finres$dest)==FALSE,]$d0) / 
  finres[is.na(finres$dest)==FALSE,]$d0 * 100
ggplot(data=finres,aes(x=SNR,y=d0,fill=error))+
  geom_raster(interpolate=FALSE)+
  scale_fill_viridis_c(limits=c(0,10),oob=scales::squish, option='B')+
  scale_x_continuous(expand=c(0,0))+
  scale_y_continuous(expand=c(0,0))+
  xlab("Signal to Noise Ratio")+
  ylab("Target size [cm]")+
  theme_classic()+
  theme(axis.text = element_text(size=16),
        axis.title = element_text(size=18))

nacount <- aggregate(data = finres, dest~SNR+d0, FUN=length)
names(nacount) <- c('SNR',"d0","count")

ggplot(data=nacount, aes(x=SNR,y=d0,fill=20-count))+geom_raster()+
  scale_fill_viridis_c(limits=c(0,10),oob=scales::squish, name="Missing", option='B')+
  scale_x_continuous(expand=c(0,0))+
  scale_y_continuous(expand=c(0,0))+
  xlab("Signal to Noise Ratio")+
  ylab("Target size [cm]")+
  theme_classic()+
  theme(axis.text = element_text(size=16),
        axis.title = element_text(size=18))




saveRDS(f,file='C:\\Users\\sveng\\OneDrive - University of Tasmania\\Nextcloud\\GitHub\\simsig\\data\\simu.RDS')
