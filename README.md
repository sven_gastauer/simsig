# simsig - Broadband acoustic signal simulator
  
Sven Gastauer (1,2); Dezhang Chu (3)  

1) Antarctic Climate and Ecosystem Cooperative Research Centre, University of Tasmania, Private Bag 80, Hobart, Tasmania, 7001, sven.gastauer@utas.edu.au
2) Australian Antarctic Division, 203 Channel Highway, Kingston, TAS 7050, Australia
3) Northwest Fisheries Science Center, National Marine Fisheries Service, National Oceanic and Atmospheric Administration, 2725 Montlake Boulevard East, Seattle, Washington 98112, USA

# Summary 
simsig is a broadband signal simulator, which can be used to estiamte the size of a target in the time or frequency domain. It simulates an acoustic broadband signal which encounters a target of a given size on its path.    
Steps completed in the simulation:  

* The time delay caused by the target is defined as ![equation](https://latex.codecogs.com/gif.latex?\frac{2*d_0&space;*&space;0.01}{c}) with *d0* the diameter of the target in cm and *c* the soundspeed in the surrounding medium  
* A chirp signal (y1) is generated for given frequencies (f0 to f1) with a set pulse duration (T) at a resolution defined by the sampling interval (dt).  
* Noise is added to the signal according to the defined signal to noise ratio (SNR) as ![equation](https://latex.codecogs.com/gif.latex?R&space;*&space;y_x&space;&plus;&space;10^{\frac{-SNR}{20}}&space;*&space;X) with ![equation](https://latex.codecogs.com/gif.latex?X&space;\sim&space;N(\mu,\sigma^2))(i.e. random sample from a normal distribution). With y1 the signal at the front interface y1n becomes the noise added front interface signal and y2 the signal at the back interface, y2n becomes the noise added back interface signal  
* The signal is weighted by a ramping function(fast or slow). Ramping is defined through a Hanning window. For slow ramping the window will be `Hanning(1)` for fast ramping, the windowing will depend on the number of samples for the back chirp  
* Pulse compression or matched filtering (R12) is achieved through cross-correlation f the base signal with the received signal (in R this is done through the base function `convolve` with `conj=TRUE, type='open'`   
* The response in the frequency domain is obtained through *|fft(R12)|*  
* Computation of the envelope of the compressed pulse through hilbert transformation (normalised with the maximum)  
* A cleaned frequency response signal is computed through removal of some of the inband noise  
* Target size estimate in the time domain as the difference between peaks in the signal response in the time domain above a threshold 

Plots included are the signal y1, y2 and y_all in the time domain, the pulse compressed signal versus the time lag, and the frequency response spectrum.

# What is this repository for?

* Simulation of a braodband signal with a target
* Check the influence of SNR on the succesful target detection
* Test the influence of input signal settings on target detection
* Compare target size estimates in the frequency and time domain

* Version 0.1

## Install simsig

*simsig* can be installed form BitBucket. This requires the [devtools](https://cran.r-project.org/web/packages/devtools/index.html) package.

``` r
# The package can be installed from BitBucket:
# install.packages("devtools")
#Install the package from bitbucket
devtools::install_bitbucket("sven_gastauer/simsig")
```
### Dependencies  

ZooScatR currently depends on: 
  
* [signal](https://cran.r-project.org/web/packages/signal/index.html) (for chirp signal generation)
* [ggplot](https://ggplot2.tidyverse.org/reference/ggplot.html) (for improved plotting capability)
* [seewave](http://rug.mnhn.fr/seewave) (for Hilbert transform)
* [shiny](https://shiny.rstudio.com/) - to build the shiny app
* [knitr](https://yihui.name/knitr/) - transparent engine for dynamic report generation with R
  
These dependencies should be installed automatically, if unavailable when ZooScatR is installed. If not, the missing libraries can be installed through:  

``` r
packages <- c("shiny", "knitr", "devtools","signal", "ggplot2")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))}
```  
If you get a message such as:  
```WARNING: Rtools is required to build R packages, but is not currently installed. ```  

Install Rtools by downloading the required binary package from cran:  

* [Windows users](https://cran.r-project.org/bin/windows)  
* [Linux users](https://cran.r-project.org/bin/linux)  
* [MacOsX](https://cran.r-project.org/bin/macosx)
* [MacOs](https://cran.r-project.org/bin/macos)  


#### Known install issues

##### Seewave  
Seewave can be installed from cran if required dependencies are met:  
`install.packages("seewave", repos="http://cran.at.r-project.org/")`  

Detailed instructions for Linux and Mac users can be found at the [seewave webpage](http://rug.mnhn.fr/seewave/inst.html).

In brief:  

* For linux users afew dependencies need to be met:  
`install.packages(c("fftw","tuneR","rgl","rpanel"), repos="http://cran.at.r-project.org/")`  
`sudo apt-get install libfftw3-3 libfftw3-dev `  
`sudo apt-get install libsndfile1 libsndfile1-dev `  
`sudo apt-get install r-cran-rgl`  
* Mac users might need to install tcltk dependencies separately  
[tcltk for mac](`http://cran.r-project.org/bin/macosx/tools/tcltk-8.5.5-x11.dmg')



# Usage - Quick Start  
  
For a quick start, a minimal example:  
``` r
library(simsig)
#run the simsig simulator
p1 <- simsig::pulseGenSize()
#with fast ramping and SNR=10 and a target with a diameter of 4.1 cm
p2 = pulseGenSize(ramp="fast", SNR=10,d0=4.1)
```

The Shiny web application can be started through:  
```r
simsig::simsigApp()
```

## Getting help  
For simsig specific questions make a feature request or post an issue on [BitBucket](https://bitbucket.org/sven_gastauer/simsig).    
For general R questions visit [Stack Overflow](https://stackoverflow.com/questions/tagged/r).  
If none of those options seem appropriate and you are getting really desperate, contact one the authors.  