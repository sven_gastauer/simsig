#' Run the signal simulator app
#' @description Runs a shiny webapp to produce a broadband signal with a target
#' @export
#' @import shiny
#' @import ggplot2
#' @import signal
#' @import seewave
#' @import plotly
#' @examples 
#' simsig::simsigApp()
#' 
source("C:\Users\\sveng\\OneDrive - University of Tasmania\\Nextcloud\\GitHub\\simsig\\R\\pulseGenSize.R")
simsigApp <- function(){
  shiny::shinyApp(
    ui <- shiny::fluidPage(

   # Application title
   titlePanel("Broadband signal simulator"),

   # Sidebar with a slider input for number of bins
   sidebarLayout(
      sidebarPanel(
        actionButton("nsm", "New simulation"),
        selectInput("ramping","Select ramping",list("Fast","Slow")),
        numericInput("dt","Sampling Interval [sec]: ",value=0.000002, min=0, max=10000,step=0.000001),
        numericInput("t0","Start time of the first echo [sec]: ",value=0.00004, min=0.0000001, max=10000000000,step=0.00001),
        numericInput("T","Pulse duration [sec]: ",value=1.0e-3, min=0.00000001, max=500,step=0.01),
        numericInput("d0","Target diameter [cm]: ",value=12, min=0.01, max=100000000000,step=1),
        numericInput("cw","Sound speed in surrounding fluid: ",value=1500, min=20, max=300000,step=1),
        numericInput("R","Amplitude Ratio: ",value=0.9, min=0.00, max=10,step=0.1),
        numericInput("SNR","Signal to Noise Ratio: ",value=10, min=-1000, max=1000,step=1),
        numericInput("f0","Start Frequency [kHz]: ",value=45, min=0.01, max=1000000,step=1),
        numericInput("f1","End Frequency [kHz]: ",value=90, min=0.01, max=1000000,step=1),
        numericInput("thresh","Threshold for peak detection: ",value=0.6, min=0.01, max=1000000,step=1)
      ),

      # Show a plot of the generated distribution
      mainPanel(
        plotOutput("timeamp"),
         plotlyOutput("sigPlot"),
        plotlyOutput("freqPlot"),
         dataTableOutput("signal")
      )
   )
),

# Define server logic required to draw a histogram
server <- function(input, output, session) {
  pp <- eventReactive(c(input$ramping,
                        input$dt,
                        input$t0,
                        input$T,
                        input$d0,
                        input$cw,
                        input$R,
                        input$SNR,
                        input$f0,
                        input$f1,
                        input$thresh),{
    pulse <- pulseGenSize(input$ramping,
                          dt=input$dt,
                          t0=input$t0,
                          T=input$T,
                          d0=input$d0,
                          cw=input$cw,
                          R=input$R,
                          SNR=input$SNR,
                          f0=input$f0,
                          f1=input$f1,
                          thresh=input$thresh,
                          plots.show=FALSE,
                          res.only=FALSE)
    
    output$signal <- renderDataTable(data.frame(pulse$res))
    #print(pulse$plotPulseCompressedTimeLag)
    output$sigPlot <- renderPlotly(print(pulse$plotPulseCompressedTimeLag))
    output$timeamp <- renderPlot(print(pulse$plotTimeAmplitude))
    output$freqPlot <- renderPlotly(print(pulse$plotFrequencyAmplitude))
                        }
  )
   # output$sigPlot <- renderPlot({
   # pp()  
   # })
   observeEvent(input$nsm, {
     pp()
   })
})

  }
